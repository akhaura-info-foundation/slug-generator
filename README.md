## URL Slug Generator

#### jQuery based instant URL Slug Generator with copy to clipboard feature.

#### Demo: https://slug-generator.netlify.com

## Copyright

© 2009-2017 [Akhaura Info Foundation](http://akhaura.info/)

Web: [http://akhaura.info](http://akhaura.info/)

Email: [service@akhaura.info](/mailto:service@akhaura.info)

## Developers

**Designed by: Sazzad Hossain Sharkar**

GitHub: [https://github.com/shsbd](https://github.com/shsbd)

## Special Thanks to
`madflow`, `speakingurl`, `clipboard.js`

## License

This program is licensed under [MIT License](https://opensource.org/licenses/MIT).